# Amazon S3 media module

This replaces the local file system provider for media with one that saves the files on Amazon S3.

## How to setup

For this to work you will need to make the following change in ~/config/FileSystemProviders.config

1. Remove the existing provider with the alias = "media"
2. Add the following:

```
<Provider alias="media" type="Gibe.Umbraco.AmazonFileSystemProvider.AmazonS3Provider, Gibe.Umbraco.AmazonFileSystemProvider">
 <Parameters>
  <add key="accessKeyId" value="** YOUR AWS ACCESS KEY HERE **" />
  <add key="secretAccessKey" value="** YOUR AWS SECRET ACCESS KEY HERE **" />
  <add key="bucketName" value="** YOUR BUCKET NAME HERE **" />
  <add key="region" value="eu-west-1" />
  <add key="mediaRoot" value="" />
  <add key="useHttps" value="1" />
 </Parameters>
</Provider>
```