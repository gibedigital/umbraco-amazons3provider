﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Gibe.Umbraco.AmazonFileSystemProvider;

// ReSharper disable once CheckNamespace
namespace Gibe.Umbraco.AmazonFileSystemProvider.Test
{
    [TestFixture]
    public class AmazonS3ProviderTest
    {
        private const string  _BUCKET_NAME = "testBucket";
        private const string  _MEDIA_ROOT = "testRoot";
        private string  _USE_HTTPS = "0";

        private AmazonS3Provider _s3Provider;

        [SetUp]
        public void SetUp()
        {
            _s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                _BUCKET_NAME, String.Empty, _MEDIA_ROOT, _USE_HTTPS);
        }

        [TestCase("0", "http")]
        [TestCase("1", "https")]
        public void HttpProtocol_IsSet_AsExpected(string useHttps, string expectedProtocol)
        {
            var s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                _BUCKET_NAME, String.Empty, _MEDIA_ROOT, useHttps);

            Assert.AreEqual(expectedProtocol, s3Provider.HttpProtocol);
        }

        [TestCase("bucket2", "bucket2.s3.amazonaws.com")]
        [TestCase("testBucket", "testBucket.s3.amazonaws.com")]
        public void S3Domain_IsSet_AsExpected(string bucket, string expectedDomain)
        {
            var s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                bucket, String.Empty, _MEDIA_ROOT, _USE_HTTPS);

            Assert.AreEqual(expectedDomain, s3Provider.S3Domain);
        }

        [TestCase(@"1001\testImage.gif", "testRoot")]
        [TestCase(@"1005\testImage_thumb.gif", null)]
        [TestCase(@"1005\testImage_thumb.gif", "")]
        public void GetFullPath_FromRelativePath_AddsMediaRoot_AsExpected(string path, string mediaRoot)
        {
            var s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                _BUCKET_NAME, String.Empty, mediaRoot, _USE_HTTPS);

            var expectedPath = String.Format("{0}{1}",
                !String.IsNullOrEmpty(mediaRoot) ? mediaRoot + "/" : String.Empty, ConvertToUrl(path));

            Assert.AreEqual(expectedPath, s3Provider.GetFullPath(path));
        }

        [TestCase(_MEDIA_ROOT, "1003/testImage.jpg")]
        [TestCase("anotherRoot", "1005/testImage.jpg")]
        public void GetFullPath_FromMediaRootedPath_DoesNot_AlterPath(string mediaRoot, string path)
        {
            var s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                _BUCKET_NAME, String.Empty, mediaRoot, _USE_HTTPS);

            var expectedPath = String.Format("{0}/{1}", mediaRoot, ConvertToUrl(path));

            Assert.AreEqual(expectedPath, s3Provider.GetFullPath(path));
        }

        [TestCase("http://testUrl.com/test")]
        [TestCase("http://testUrl1.com/test5")]
        public void GetFullPath_FromHttpPath_DoesNot_AlterPath(string path)
        {
            var s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                _BUCKET_NAME, String.Empty, _MEDIA_ROOT, _USE_HTTPS);

            Assert.AreEqual(path, s3Provider.GetFullPath(path));
        }

        [TestCase(@"1001\testImage.gif", "")]
        [TestCase(@"1005\testImage_thumb.gif", "")]
        public void GetUrl_ConvertsFilePath_AsExpected(string filePath, string path)
        {
            var s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                _BUCKET_NAME, String.Empty, _MEDIA_ROOT, _USE_HTTPS);

            var expectedPath = String.Format("{0}://{1}/{2}/{3}", 
                s3Provider.HttpProtocol, s3Provider.S3Domain, _MEDIA_ROOT,
                ConvertToUrl(filePath));
            Assert.AreEqual(expectedPath, s3Provider.GetUrl(filePath));
        }

        [TestCase(@"/", _MEDIA_ROOT, "/" + _MEDIA_ROOT + "/")]
        [TestCase(@"media/1002/test.gif", "media", "/media/1002/test.gif")]
        [TestCase(@"1002/test.gif", _MEDIA_ROOT, "/" + _MEDIA_ROOT + "/1002/test.gif")]
        [TestCase(@"1002/test.gif", "", "/1002/test.gif")]
        public void GetUrl_BuildsUrl_AsExpected(string filePath, string mediaRoot, string expectedPath)
        {
            var s3Provider = new AmazonS3Provider(String.Empty, String.Empty,
                _BUCKET_NAME, String.Empty, mediaRoot, _USE_HTTPS);

            var expectedFullPath = String.Format("{0}://{1}{2}",
                    s3Provider.HttpProtocol, s3Provider.S3Domain, expectedPath);

                Assert.AreEqual(expectedFullPath, s3Provider.GetUrl(filePath));
        }

        [TearDown]
        public void TearDown()
        {
            _s3Provider = null;
        }


        private string ConvertToUrl(string input)
        {
            return  input.Replace(Path.DirectorySeparatorChar, '/');
        }
    }
}
